<?php

declare(strict_types=1);

namespace Recruitment\Entity;

class Product
{
	private $unit;
	private $minQuantity;
	private $id;
	private $quantity;
	
	
	public function setUnitPrice($unit)
	{
		if($unit <= 0)
		{
			throw new \Recruitment\Entity\Exception\InvalidUnitPriceException;
		}
		else
		{
			$this->unit = $unit;
		}
	
	}
	public function getUnitPrice()
	{
		return $this->unit;
	}
	public function setId($id)
	{
		$this->id = $id;
	}
	public function getId()
	{
		return $this->id;
	}
	
	public function getTotalPrice()
	{
		return $this->unit * $this->quantity;
	}
	public function setMinimumQuantity($minQuantity)
	{
		if($minQuantity <= 0)
		{
			throw new \InvalidArgumentException;
		}
		else
		{
			$this->minQuantity = $minQuantity;

		}
	}
	public function getMinimumQuantity()
	{
		return $this->minQuantity;
	}
	public function setQuantity()
	{
		$this->quantity = quantity;
	}
	public function getQuantity()
	{
		return $this->quantity;
	}
	
}
