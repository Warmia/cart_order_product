<?php

declare(strict_types=1);

namespace Recruitment\Cart;

use Recruitment\Entity\Product as Product;

class Item
{
	private $product;
	
	public function __construct(Product $product, $quantity)
	{
		$this->product = $product;
		
		$this->product->setQuantity($quantity);
		
		if($this->product->getMinimumQuantity() > $quantity)
		{
			throw new \InvalidArgumentException;
		}
		
	}
	
	
	public function getProduct()
	{
		return $this->product;
	}
	public function getQuantity()
	{
		return $this->getProduct()->getQuantity();
	}
	public function setQuantity($quantity)
	{
		if($this->getProduct()->getMinimumQuantity() > $quantity)
		{
			throw new \Recruitment\Cart\Exception\QuantityTooLowException;
		}
		else
		{
			$this->getProduct()->setQuantity($quantity);
		}
		
	}
	public function getTotalPrice( )
	{
		return $this->getProduct()->getTotalPrice();
	}
	
	
	
	
	
}
