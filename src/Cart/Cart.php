<?php

declare(strict_types=1);

namespace Recruitment\Cart;

use Recruitment\Cart\Cart;
use Recruitment\Entity\Order as Order;
use Recruitment\Entity\Product as Product;

class Cart
{
	
	
	
	private $product;
	private $cartArray = [];
	private $id;
	
	public function getArray()
	{
		return $this->cartArray;
	}
	public function addProduct(Product $product, $position )
	{
		$product->setQuantity($product->getQuantity+1);
		$cartArray[$position] = $product;
	}
	
	public function getItems()
	{
		return count($cartArray);
	}
	public function getTotalPrice()
	{
		$sum = 0;
		foreach($cartArray as $element)
		{
			$sum += $element -> getTotalPrice();
		}
	}
	
	
	public function getItem($position)
	{
		return $cartArray[$position];
		if($cartArray[$position] == null)
		{
			throw new \OutOfBoundsException;
		}
	}
	public function getProduct()
	{
		return self();
	}
	public function removeProduct(Product $product)
	{
		$key = array_search($product, $cartArray);
		unset($cartArray[$key]);
	}
	
	public function checkout($id)
	{
		$this->id = $id;
		return new Order();
	}
	public function getId()
	{
		return $this->id;
	}
	
   
}
